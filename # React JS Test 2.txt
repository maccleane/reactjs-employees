# React JS Test 2

## Instructions
1. Complete the test, commit your code into a github repository and send us a link to the repository.
2. Max time for the test is 4 hours.
3. Use React JS with Hooks.
4. Use LESS or SASS for styling - optional.
5. Use a UI framework of your choice - optional.

## Requirements
Teknorix wishes to build a ReactJS application to display their employess data. 

The application should show a list of all active employees with search/filter/delete functionality. Employee details should be shown on a unique url.

### Search 
1. Search - Search box to search for employees.
2. Teams - Dropdown to show a list of all teams. 
3. Location - Dropdown to show a list of all locations. 
5. Show applied filters with a X button to remove the filter.
6. Retain the applied filters on navigation to details page and back or refresh of page.
7. Use the lookups API to load the above.
8. Use server side filtering - applied filters must be passed to the api to load the list of employees.

### List
![List Page](/assets/list.jpg)
1. Implement list page as per the mockup above.
2. View button - navigate to the details page of the employee.
3. Use the /api/v1/employees GET API to load the list of employees.

### Details
![Details Page](/assets/details.jpg)
1. Implement details page as per the mockup above.
2. Use the /api/v1/employees/{id} GET API to load the details of the employee.

### AddNew
![AddNew Page1](/assets/basic.jpg)
![AddNew Page2](/assets/employment.jpg)
![AddNew Page3](/assets/personal.jpg)
1. Implement add new page as per the mockup above.
2. Use the /api/v1/employees POST API to load the details of the employee.

### Dropdown Apis
1. Teams => /teams
2. JobTitle => /job-title


## API Documentation
> https://localhost:5001/index.html

## Base API URL
>https://localhost:5001/

##Api project in .NET6