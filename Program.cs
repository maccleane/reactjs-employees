using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.ComponentModel.DataAnnotations;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddDbContext<TeamsDbContext>(options => options.UseInMemoryDatabase("Employees"));
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(setup => setup.SwaggerDoc("v1", new OpenApiInfo()
{
    Description = "Employees api",
    Title = "Employees api",
    Version = "v1",
    Contact = new OpenApiContact()
    {
        Name = "teknorix",
        Url = new Uri("https://teknorix.com")
    }
}));
var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
app.UseSwagger();
//employees
app.MapGet("/employees", async ([FromServices] TeamsDbContext dbContext, string searchTerm, string team, string jobTitle) =>
{

    var query = dbContext.Employees.Where(s => s.IsActive == true);
    if (!string.IsNullOrWhiteSpace(searchTerm))
    {
        query = query.Where(s => s.FirstName.Contains(searchTerm) || s.LastName.Contains(searchTerm));
    }
    if (!string.IsNullOrWhiteSpace(jobTitle))
    {
        query = query.Where(s => s.JobTitle.ToLower() == jobTitle.ToString().ToLower());
    }
    if (!string.IsNullOrWhiteSpace(team))
    {
        query = query.Where(s => s.Team.ToLower() == team.ToString().ToLower());
    }
    var result = await query.ToListAsync();
    return Results.Ok(result);
});

app.MapGet("/employees/{id}", async ([FromServices] TeamsDbContext dbContext, int id) =>
{
    var employee = await dbContext.Employees.FindAsync(id);
    return employee;
});

app.MapPost("/employees", async ([FromServices] TeamsDbContext dbContext, EmployeeDTO updatedEmployee) =>
{
    var employee = new Employee();
    employee.Address = updatedEmployee.Address;
    employee.Avatar = updatedEmployee.Avatar;
    employee.DateOfBirth = updatedEmployee.DateOfBirth;
    employee.DateOfJoining = updatedEmployee.DateOfJoining;
    employee.Email = updatedEmployee.Email;
    employee.FirstName = updatedEmployee.FirstName;
    employee.LastName = updatedEmployee.LastName;
    employee.JobTitle = updatedEmployee.JobTitle;
    employee.Location = updatedEmployee.Location;
    employee.MaritalStatus = updatedEmployee.MaritalStatus;
    employee.Nationality = updatedEmployee.Nationality;
    employee.Phone = updatedEmployee.Phone;
    employee.Prefix = updatedEmployee.Prefix;
    employee.SupervisorId = updatedEmployee.SupervisorId;
    employee.Team = updatedEmployee.Team;
    employee.EmploymentType = employee.EmploymentType;
    employee.EmployeeCode = employee.EmployeeCode;
    employee.CreatedBy = 1;
    employee.CreatedAt = DateTime.UtcNow;
    dbContext.Employees.Add(employee);
    await dbContext.SaveChangesAsync();
    return Results.Created($"/employees/{employee.Id}", employee);
});

app.MapPut("/employees/{id}", async ([FromServices] TeamsDbContext dbContext, int id, EmployeeDTO updatedEmployee) =>
{
    var employee = await dbContext.Employees.FirstOrDefaultAsync(s => s.Id == id);
    if (employee == null)
    {
        return Results.NotFound();
    }
    employee.Address = updatedEmployee.Address;
    employee.Avatar = updatedEmployee.Avatar;
    employee.DateOfBirth = updatedEmployee.DateOfBirth;
    employee.DateOfJoining = updatedEmployee.DateOfJoining;
    employee.Email = updatedEmployee.Email;
    employee.FirstName = updatedEmployee.FirstName;
    employee.LastName = updatedEmployee.LastName;
    employee.JobTitle = updatedEmployee.JobTitle;
    employee.Location = updatedEmployee.Location;
    employee.MaritalStatus = updatedEmployee.MaritalStatus;
    employee.Nationality = updatedEmployee.Nationality;
    employee.Phone = updatedEmployee.Phone;
    employee.Prefix = updatedEmployee.Prefix;
    employee.SupervisorId = updatedEmployee.SupervisorId;
    employee.Team = updatedEmployee.Team;
    employee.EmploymentType = employee.EmploymentType;
    employee.EmployeeCode = employee.EmployeeCode;
    employee.ModifiedBy = 1;
    employee.CreatedAt = DateTime.UtcNow;
    await dbContext.SaveChangesAsync();
    return Results.Status(204);
});

app.MapDelete("/employees/{id}", async ([FromServices] TeamsDbContext dbContext, int id) =>
{
    var employee = await dbContext.Employees.FirstOrDefaultAsync(s => s.Id == id);
    if (employee == null)
    {
        return Results.NotFound();
    }

    dbContext.Employees.Remove(employee);
    await dbContext.SaveChangesAsync();

    return Results.Status(204);
});

app.MapGet("/teams", async (TeamsDbContext dbContext) =>
{
    var list = await dbContext.Employees.ToListAsync();
    var result = list.Select(s => s.Team).Distinct().Select(x => new Lookup
    {
        Text = x,
        Value = x

    }).ToList();
    return Results.Ok(result);
});

app.MapGet("/job-title", async (TeamsDbContext dbContext) =>
{
    var list = await dbContext.Employees.ToListAsync();
    var result = list.Select(s => s.JobTitle).Distinct().Select(x => new Lookup
    {
        Text = x,
        Value = x

    }).ToList();
    return Results.Ok(result);
});

app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Employees Api v1");
    c.RoutePrefix = string.Empty;
});
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Employees Api v1");
    c.RoutePrefix = string.Empty;
});
app.Run();


public class TeamsDbContext : DbContext
{
    public TeamsDbContext(DbContextOptions options) : base(options)
    {
        this.Init();
    }

    protected TeamsDbContext()
    {
    }
    public DbSet<Employee> Employees { get; set; }

    public void Init()
    {
        var employees = new List<Employee>
            {
            new Employee
            {
                Prefix = "Mr",
                FirstName = "Joffrey",
                LastName = "Baratheon",
                Gender = "Male",
                Email = "jfo@teknorix.com",
                Address = "4576 Bastin Drive, Philadelphia",
                Avatar = "https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50",
                DateOfBirth = new DateTime(1989,02,05),
                DateOfJoining = new DateTime(2010, 06, 05),
                EmployeeCode = "EMP100",
                EmploymentType = "Full-Time",
                IsActive = true,
                JobTitle = "Project Manager",
                MaritalStatus = "Single",
                Phone = "+14458203377",
                Nationality = "US",
                Location="The Crownlands",
                Team="Development",
                SupervisorId = null,
                CreatedAt = DateTime.UtcNow,
                CreatedBy = 1

            },
             new Employee
            {
                Prefix = "Mr",
                FirstName = "Jamimie",
                LastName = "Lannister",
                Gender = "Male",
                Email = "king@teknorix.com",
                Address = "3420 Sc 19 Hwy, Trenton, SC, 29847  ",
                Avatar = "https://i.pravatar.cc/150?img=3",
                DateOfBirth = new DateTime(1989,02,05),
                DateOfJoining = new DateTime(2010, 06, 05),
                EmployeeCode = "EMP101",
                EmploymentType = "Full-Time",
                IsActive = true,
                JobTitle = "Team Lead",
                MaritalStatus = "Single",
                Phone = "+14458203347",
                Nationality = "US",
                SupervisorId = 1,
                Team="Development",
                Location="The Crownlands",
                CreatedAt = DateTime.UtcNow,
                CreatedBy = 1

            },
                new Employee
            {
                Prefix = "Mrs",
                FirstName = "Margaery",
                LastName = "Tyrell",
                Gender = "Female",
                Email = "try@teknorix.com",
                Address = "115 N Wood St, Loudonville, OH, 44842",
                Avatar = "https://i.pravatar.cc/150?img=1",
                DateOfBirth = new DateTime(1988,07,09),
                DateOfJoining = new DateTime(2012, 01, 01),
                EmployeeCode = "EMP102",
                EmploymentType = "Full-Time",
                IsActive = true,
                JobTitle = "Sales Manager",
                MaritalStatus = "Single",
                Phone = "+144582033437",
                Nationality = "US",
                Team="Marketing",
                Location="The Vale of Arrynn",
                SupervisorId = 1,
                CreatedAt = DateTime.UtcNow,
                CreatedBy = 1

            },
                new Employee
            {
                Prefix = "Mrs",
                FirstName = "Olenna",
                LastName = "Baratheon",
                Gender = "Female",
                Email = "ole@teknorix.com",
                Address = "10344 Kerns Rd, Huntersville, NC, 28078  ",
                Avatar = "https://i.pravatar.cc/150?img=5",
                DateOfBirth = new DateTime(1990,06,21),
                DateOfJoining = new DateTime(2011, 09, 11),
                EmployeeCode = "EMP103",
                EmploymentType = "Full-Time",
                IsActive = true,
                JobTitle = "Designer",
                MaritalStatus = "Single",
                Phone = "+14458203477",
                Nationality = "US",
                Location = "The Reach",
                SupervisorId = 1,
                Team="Design",
                CreatedAt = DateTime.UtcNow,
                CreatedBy = 1

            },
                new Employee
            {
                Prefix = "Mr",
                FirstName = "Sandor",
                LastName = "Clegan",
                Gender = "Male",
                Email = "san@teknorix.com",
                Address = "27425 Sweet Springs Rd, Ardmore, AL, 35739",
                Avatar = "https://i.pravatar.cc/150?img=5",
                DateOfBirth = new DateTime(2000,11,3),
                DateOfJoining = new DateTime(2020, 04, 02),
                EmployeeCode = "EMP104",
                EmploymentType = "Full-Time",
                IsActive = true,
                JobTitle = "Designer",
                MaritalStatus = "Single",
                Phone = "+14458203477",
                Team="Design",
                Nationality = "US",
                Location = "The Westerlands",
                SupervisorId = 1,
                CreatedAt = DateTime.UtcNow,
                CreatedBy = 1

            },
               new Employee
            {
                Prefix = "Mr",
                FirstName = "Devin ",
                LastName = "Smith",
                Gender = "Male",
                Email = "dev@teknorix.com",
                Address = " 1029 E 1st St, Rural Valley, PA, 16249",
                Avatar = "https://i.pravatar.cc/150?img=6",
                DateOfBirth = new DateTime(1989,03,03),
                DateOfJoining = new DateTime(2004, 12, 12),
                EmployeeCode = "EMP106",
                EmploymentType = "Full-Time",
                IsActive = true,
                JobTitle = "Operation Manager",
                Team="Design",
                MaritalStatus = "Marred",
                Phone = "+14458202477",
                Nationality = "US",
                Location = "The Westerlands",
                SupervisorId = 1,
                CreatedAt = DateTime.UtcNow,
                CreatedBy = 1

            },
                    new Employee
            {
                Prefix = "Mrs",
                FirstName = "Roberta",
                LastName = "Bealish",
                Gender = "Female",
                Email = "rob@teknorix.com",
                Address = "115 72nd St, Ocean City, MD, 21842",
                Avatar = "https://i.pravatar.cc/150?img=9",
                DateOfBirth = new DateTime(2000,11,3),
                DateOfJoining = new DateTime(2020, 05, 11),
                EmployeeCode = "EMP107",
                EmploymentType = "Full-Time",
                IsActive = true,
                JobTitle = "Developer",
                MaritalStatus = "Single",
                Phone = "+14458203477",
                Team="Development",
                Nationality = "US",
                Location = "The Westerlands",
                SupervisorId = 1,
                CreatedAt = DateTime.UtcNow,
                CreatedBy = 1

            },
              new Employee
            {
                Prefix = "Mr",
                FirstName = "Carey",
                LastName = "Smith",
                Gender = "Male",
                Email = "can@teknorix.com",
                Address = "5361 Apolina Via, Yorba Linda, CA, 92886",
                Avatar = "https://i.pravatar.cc/150?img=7",
                DateOfBirth = new DateTime(2000,12,12),
                DateOfJoining = new DateTime(2020, 06, 02),
                EmployeeCode = "EMP108",
                EmploymentType = "Full-Time",
                IsActive = true,
                JobTitle = "Sales Manager",
                Team="Marketing",
                MaritalStatus = "Single",
                Phone = "+14458203477",
                Nationality = "US",
                Location = "The Westerlands",
                SupervisorId = 1,
                CreatedAt = DateTime.UtcNow,
                CreatedBy = 1

            },
              new Employee
            {
                Prefix = "Mr",
                FirstName = "Michael",
                LastName = "Smith",
                Gender = "Male",
                Email = "mic@teknorix.com",
                Address = "386 Pete Ave, Waynesville, GA, 31566 ",
                Avatar = "https://i.pravatar.cc/150?img=8",
                DateOfBirth = new DateTime(1999,07,19),
                DateOfJoining = new DateTime(2019, 08, 08),
                EmployeeCode = "EMP109",
                EmploymentType = "Full-Time",
                IsActive = true,
                JobTitle = "Project Manager",
                Team="Development",
                MaritalStatus = "Single",
                Phone = "+144458203477",
                Nationality = "US",
                Location = "The Westerlands",
                SupervisorId = null,
                CreatedAt = DateTime.UtcNow,
                CreatedBy = 1

            },
            new Employee
            {
                Prefix = "Mr",
                FirstName = "Carson",
                LastName = "Clegan",
                Gender = "Female",
                Email = "ccc@teknorix.com",
                Address = "3202 NE 168th Ave, Vancouver, WA, 98682",
                Avatar = "https://i.pravatar.cc/150?img=11",
                DateOfBirth = new DateTime(2001,09,03),
                DateOfJoining = new DateTime(2018, 07, 07),
                EmployeeCode = "EMP110",
                EmploymentType = "Full-Time",
                IsActive = true,
                JobTitle = "Sales Executive",
                MaritalStatus = "Single",
                Phone = "+144584403477",
                Nationality = "US",
                Location = "The Reach",
                Team="Marketing",
                SupervisorId = 1,
                CreatedAt = DateTime.UtcNow,
                CreatedBy = 1
             }
            };

        foreach (var item in employees)
        {
            this.Employees.Add(item);
            this.SaveChanges();
        }

    }
}
public class Employee
{
    [Key]
    public int Id { get; set; }
    public int? CreatedBy { get; set; }
    public DateTime CreatedAt { get; set; }
    public int? ModifiedBy { get; set; }
    public DateTime? ModifiedAt { get; set; }
    public string Avatar { get; set; }
    public string Prefix { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Gender { get; set; }
    public DateTime? DateOfBirth { get; set; }
    public string MaritalStatus { get; set; }
    public string Nationality { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    public string Address { get; set; }
    public string JobTitle { get; set; }
    public string EmployeeCode { get; set; }
    public string Location { get; set; }
    public DateTime? DateOfJoining { get; set; }
    public string EmploymentType { get; set; }
    public int? SupervisorId { get; set; }
    public string Team { get; set; }
    public virtual Employee Supervisor { get; set; }
    public bool IsActive { get; set; }
    public bool IsDeleted { get; set; }
    public int? DeletedBy { get; set; }
    public DateTime? DeletedAt { get; set; }

}

public class EmployeeDTO
{
    public int Id { get; set; }
    public string Avatar { get; set; }
    public string Prefix { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Gender { get; set; }
    public DateTime? DateOfBirth { get; set; }
    public string MaritalStatus { get; set; }
    public string Nationality { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    public string Address { get; set; }
    public string JobTitle { get; set; }
    public string EmployeeCode { get; set; }
    public string Location { get; set; }
    public DateTime? DateOfJoining { get; set; }
    public string EmploymentType { get; set; }
    public int? SupervisorId { get; set; }
    public string Team { get; set; }

}

public class Lookup
{
    public string Text { get; set; }
    public string Value { get; set; }
}
public static class Results
{
    public static ActionResult NotFound() => new StatusCodeResult(404);
    public static ActionResult Ok() => new StatusCodeResult(200);
    public static ActionResult Status(int statusCode)
        => new StatusCodeResult(statusCode);
    public static JsonResult Ok(object value) => new JsonResult(value);
    public static IResult Created(string location, object value)
        => new CreatedResult(location, value);

    private class CreatedResult : IResult
    {
        private readonly object _value;
        private readonly string _location;

        public CreatedResult(string location, object value)
        {
            _location = location;
            _value = value;
        }

        public Task ExecuteAsync(HttpContext httpContext)
        {
            httpContext.Response.StatusCode = StatusCodes.Status201Created;
            httpContext.Response.Headers.Location = _location;

            return httpContext.Response.WriteAsJsonAsync(_value);
        }
    }
}